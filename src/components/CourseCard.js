
//activity

//import {Row, Col,Card, Button} from 'react-bootstrap';

//export default function CourseCard(){
//    return(
//        <Row className="my-3">
//            <Col xs={12} md={12}>
//                <Card className="cardHighlight p-3">
 //                   <Card.Body>
 //                       <Card.Title>
 //                           <h2>Sample Course</h2>
 //                           <h3>Description</h3>
 //                               This is a sample course offering.
  //                      </Card.Title>
   //                     <Card.Text>
   //                         Price:
    //                        <p>Php 40,000</p>
    //                       <Button variant="primary">Enroll</Button>                
     //                   </Card.Text>
      //              </Card.Body>
                    
      //          </Card>
                
       //     </Col>
                
       // </Row>
   // )
//}

import {useState, useEffect} from 'react';
import { Card, Button } from 'react-bootstrap';
//destructuring is done in the parameter to retrieve the 


export default function CourseCard({courseProp}) {
    
    //console.log(props.courseProp);
   // console.log(courseProp);

    const{name, description, price} = courseProp;

    //use the state hoo for this component to be able to store its state
    //states are used to keep track of information related to individual components
    //syntax:
        //conts [getter. setter] = useState(initialGetterValue)

    //when a component mounts (loads for the first time), any associated states will undergo a state from null to the given initial/default state

    //e.g. count below goes from null to 0, since 0 is our initial state
        //count,setCount is a naming convention
    const[count, setCount] = useState(0);
    const[seats, setSeats] = useState(30);


    //using the state hook returns an array with the first element being a value and the second element as a function that's used to change the value of the first element

    console.log(useState(0));

    //function that keeps track of the enrollees for a course 
    //by default JavaScript is synchronous, as it executes code from the top of the file all the way to the bottom and will wait for the completion of one expression before it proceeds to the next
    //the setter function for useStates are asynchronous, allowing it to execute separately from other codes in the program
    //the "setCount" function is being executed while the "console.log" is already being completed resulting in the console to be behind by one count.

    function enroll (){
       // if (seats > 0 ){
        setCount(count + 1);
      //  console.log('Enrollees:' - 1);
        setSeats(seats - 1);
      //  console.log('Seats:' + seats)
   // }  //else {
       // alert("No more seats available");
   // }
};

    //use effect makes any given code block happen when a state changes AND when a components first mounts (such as on initial page load)
    //ine the example below, since count and seats are in the array of our useState, the code block will execute whenever those states change
    //if the array is blank, the code will be executed on component mount ONLY
    //do not omit the array completely, or 

    //syntax
    /*
        useEffect(() => {
            code to be executed
        }), [state(s) to monitor])
    */

    useEffect(() => {
        if(seats === 0){
            alert('No more seats available')
        }
    }, [count, seats])


    return (
        <Card className = "mb-2">
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>Php {price}</Card.Text>
                <Card.Text>Enrollees: {count}</Card.Text>
                <Card.Text>Seats: {seats}</Card.Text>
                <Button variant='primary' onClick={enroll}>Enroll</Button>

            </Card.Body>
        </Card>
    )
}