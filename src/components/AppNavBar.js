//long method
//import Container from 'react-bootstrap/Container';
//import Nav from 'react-bootstrap/Nav';
//import Navbar from 'react-bootstrap/Navbar';
//import NavDropdown from 'react-bootstrap/NavDropdown';

//short method
//tinanggal ko NavDropdown

import {useContext} from 'react';
import {Container, Nav, Navbar} from 'react-bootstrap'
import {Link} from 'react-router-dom';
import UserContext  from '../UserContext';

export default function AppNavBar(){

	//a context object such as our UserContext can be "opened" with React's useContext hook
	const { user } = useContext(UserContext);

	return(
		 <Navbar bg="light" expand="lg">
      		<Container>
      			<Link className="navbar-brand" to="/">Zuitt</Link>
       			 <Navbar.Toggle aria-controls="basic-navbar-nav" />
       			 <Navbar.Collapse id="basic-navbar-nav">
       	{/*mr -> me
			ml -> ms
       	 */
       	}
         		 <Nav className="ms-auto">
          			  <Link className="nav-link" to="/">Home</Link>
           			 <Link className="nav-link" to="/courses" exact>Courses</Link>

           			 {(user.email !==null) ?

           			 	
           			 	<Link className="nav-link" to="/logout">Logout</Link>
           			 :

           			 <>
           			 	<Link className="nav-link" to="/login">Login</Link>
           			    <Link className="nav-link" to="/register">Register</Link>
           			 </>

           			 }

           			 
         			</Nav>
        			</Navbar.Collapse>
     		 </Container>
   		 </Navbar>

	)
}