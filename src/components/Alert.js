import Alert from 'react-bootstrap/Alert';

function Alert() {
  return (
    <>
      {[
        
        'light',
        
      ].map((variant) => (
        <Alert key={variant} variant={variant}>
          No more Seats Available!
        </Alert>
      ))}
    </>
  );
}

export default Alert;