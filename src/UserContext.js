import React from 'react';

//creates a React context object
//a context object contains data that can be passed to multiple props
//think of it like a delivery container or a box
const UserContext = React.createContext();

//a provider is what is used to distribute the context object to the components
export const UserProvider = UserContext.Provider;

export default UserContext;

