import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

//activity assignment
//import { Redirect} from 'react-router-dom';
//activity assignment
        /*
        <Redirect to="/login"/> 
        */

export default function Home(){


	const data = {
		title: "Zuitt Coding Bootcamp",
		content: "Opportunities for everyone, everywhere",
		destination: "/courses",
		label: "Enroll now!"
	}

	
	return(
	<>	
		<Banner dataProp = {data}/>
        <Highlights />  

        
    </>
    
    	
   
	)
}