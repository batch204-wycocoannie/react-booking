import {useState, useEffect,  useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import {Redirect} from 'react-router-dom';
import UserContext from '../UserContext';
export default function Register(){

	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");
	const [isActive, setisActive] = useState(false);

	const {user} = useContext(UserContext);


	/*
		/to properly change and save input values, we must implement two-way binding we need to capture whatever the user types in the input a sthey are typing 
		/meaning we need the input's .value value
		/to get the .value, we capture the event  (in this case, OnChange). the target of the onChange event is the input, meaniong we can get the .value
	*/

	useEffect(() => {
		//console.log(email)
		//console.log(password1)
		//console.log(password2)

		if((firstName !== '' && lastName !== '' && email !== '' && mobileNo.length === 11 && password1 !== '' && password2 !== '') && (password1 === password2))  {
				setisActive(true)
		} else {
				setisActive(false)
		}

	}, [firstName, lastName, email, mobileNo, password1, password2])

	function registerUser(e){
		e.preventDefault()  //prevent default form behavior, so that the form does not submit

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: "POST",
			headers:{
				'Content-Type': 'application/json'
			}, 
			body: JSON.stringify({
				email: email
			})
		})
		//nilalagay ang .then after ng fetch
		.then(res => res.json())
		.then(data => {
			if(data){
				alert("Duplicate email exists")
			} else {
				fetch(`${process.env.REACT_APP_API_URL}/users/register`,{
					method: "POST",
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNo,
						password: password1
					})
				})
				.then(res => res.json())
				.then(data => {
					if(data){
						alert("User succesfuly registered")
					} else{
						alert("Something went wrong")
					}
				})
			}
		})


		//alert('Thank you for registering');
	}

	return(
		(user.email !==null) ?	
		<Redirect to ="/"/>
		:
		
		<Form onSubmit={e => registerUser(e)}>

			<Form.Group controlId="firstName">
				<Form.Label>First Name</Form.Label>
				<Form.Control
					type="text"
					placeholder = "Enter First Name"
					value={firstName}
					onChange={e => setFirstName(e.target.value)}
					required
				/>			
			</Form.Group>

			<Form.Group controlId="lastName">
				<Form.Label>Last Name</Form.Label>
				<Form.Control
					type="text"
					placeholder = "Enter Last Name"
					value={lastName}
					onChange={e => setLastName(e.target.value)}
					required
				/>
				
			</Form.Group>

			<Form.Group controlId="email">
				<Form.Label>Email</Form.Label>
				<Form.Control
					type="email"
					placeholder = "Enter your email"
					value={email}
					onChange={e => setEmail(e.target.value)}
					required
				/>
				
			</Form.Group>

			<Form.Group controlId="mobileNo">
				<Form.Label>Mobile Number</Form.Label>
				<Form.Control
					type="text"
					placeholder = "Enter mobile number"
					value={mobileNo}
					onChange={e => setMobileNo(e.target.value)}
					required
				/>
				
			</Form.Group>

			<Form.Group controlId="password1">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder = "Enter Password"
					value ={password1}
					onChange={e => setPassword1(e.target.value)}
					required
				/>				
			</Form.Group>

			<Form.Group controlId="password2">
				<Form.Label>Verify Password</Form.Label>
				<Form.Control
					type="password"
					placeholder = "Verify Password"
					value = {password2}
					onChange={e => setPassword2(e.target.value)}
					required
				/>
				
			</Form.Group>

			{isActive ? 
				<Button className="mt-3" variant="primary" type="submit" id="submitBtn">
				Submit
				</Button>
				:
				<Button className="mt-3" variant="primary" id="disabled">
				Submit
			</Button>

			}
			
		</Form>
		)
}