import { useState, useEffect, useContext} from 'react';

import {Form, Button} from 'react-bootstrap';
import UserContext from '../UserContext';
import {Redirect} from 'react-router-dom';

export default function Login(){

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	//const [password2, setPassword2] = useState("");
	const [isActive, setisActive] = useState(false);

	const { user, setUser} = useContext(UserContext);

	
	/*
		/to properly change and save input values, we must implement two-way binding we need to capture whatever the user types in the input a sthey are typing 
		/meaning we need the input's .value value
		/to get the .value, we capture the event  (in this case, OnChange). the target of the onChange event is the input, meaniong we can get the .value
	*/

	useEffect(() => {
		//console.log(email)
		//console.log(password1)
		//console.log(password2)

		if(email !== '' && password !== '' )      {
				setisActive(true)
		} else {
				setisActive(false)
		}

	}, [email, password])

	function authenticateUser(e){
		e.preventDefault()  //prevent default form behavior, so that the form does not submit

		//localStorage.setItem allows us to save a key/value pair to localStorage
		//syntax localStorage.setItem('email',email)
		
//ACTIVITY: 
/*create a fetch request inside this function to allow users login
log in the console the response no need for the alert*/
fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: "POST",
			headers:{
				'Content-Type': 'application/json'
			}, 
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		
		.then(res => res.json())
		.then(data => {
			console.log(data);
			});


		//localStorage.setItem('email', email)


		//setUser({
	//		email: email
	//	})

	//	setEmail("")
	//	setPassword("")
		//setPassword2("")

	
	//	alert('You are now login')
	
	}

	return(
		(user.email !==null) ?	
		<Redirect to ="/"/>
		:
		<Form onSubmit={e => authenticateUser(e)}>
			<Form.Group controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder = "Enter email"
					value={email}
					onChange={e => setEmail(e.target.value)}
					required
				/>
				
			</Form.Group>

			<Form.Group controlId="password">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder = "Enter Password"
					value ={password}
					onChange={e => setPassword(e.target.value)}
					required
				/>
				
			</Form.Group>

		
			{isActive ? 
				<Button className="mt-3" variant="primary" type="submit" id="submitBtn" >
				Submit
				</Button>
				:
				<Button className="mt-3" variant="primary" id="submitBtn" disabled>
				Submit
			</Button>

			}





			
		</Form>
		)
}